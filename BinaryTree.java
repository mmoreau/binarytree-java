public class BinaryTree {
	
	// .:: Private Attributs Class ::.

	private static int count = 0;
	
	
	// .:: Private Attributs Objet ::.

    private int value;
    private BinaryTree left, right;
    
    
   	// .:: Constructeurs ::.

    public BinaryTree(int v) {
        value = v;
    }


    public BinaryTree(int v, BinaryTree l, BinaryTree r) {
        value = v;
        left = l;
        right = r;
    } 

    
	// .:: Accesseurs ::.
	
    public int getValue() {
        return value;
    }

    
    public BinaryTree getSousArbreGauche() {
        return left;
    }

    
    public BinaryTree getSousArbreDroit() {
        return right;
    }

    
    /* ----- METHODS CLASS ----- */
	
	
    // .:: Sum each values of tree ::.
	public static int sum(BinaryTree tree) {

		if (tree == null) {
    		return 0;
    	} else {
    		return sum(tree.left) + sum(tree.right) + tree.value;
		}
    }

    
    // .:: Checks if the tree is a leaf ::.
    public static boolean isSheet(BinaryTree tree) {

    	if ((tree.left == null) && (tree.right == null)) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    
    // .:: Search value max of tree ::. 
    public static int max(BinaryTree tree) {

		int max = tree.value;

    	if (tree.left != null) {
    		max = Math.max(max, max(tree.left));
    	}
    	
    	if (tree.right != null) {
    		max = Math.max(max, max(tree.right));
		}
    	
    	return max;
    }
    
    
    // .:: Search value min of tree ::.
    public static int min(BinaryTree tree) {
    	    	    	
    	int min = tree.value;

    	if (tree.left != null) {
    		min = Math.min(min, min(tree.left));
    	}
    	
    	if (tree.right != null) {
    		min = Math.min(min, min(tree.right));
    	}
    	
    	return min;
    }
    
    
    public static void prefixe(BinaryTree tree) {
		
    	if (tree != null) {

    		System.out.println(tree.value);
    		prefixe(tree.left);
    		prefixe(tree.right);
    	}
    }
    
    
    public static void infixe(BinaryTree tree) {

    	if (tree != null) {

    		infixe(a.left);
    		System.out.println(tree.value);
    		infixe(a.right);
    	}
    }
    
    
    public static void postfixe(BinaryTree tree) {

    	if (tree != null) {

    		postfixe(tree.left);
    		postfixe(tree.right);
    		System.out.println(tree.value);
    	}
    }
    
    // .:: Returns the depth of the tree ::.
    public static int height(BinaryTree tree) {

    	if (tree == null) {
    		return 0;
    	} else {
    		return 1 + (Math.max(height(tree.left), height(tree.right)));
    	}
    }
    
    
    public static boolean valueExists(BinaryTree tree, int value) {
    	
    	if (value == tree.value) {
		    return true;
		}
		    	
		if (tree.left != null) {
		    return valueExists(tree.left, value);
		}
		    	
		if (tree.right != null) {
			return valueExists(tree.right, value);
		}
    
	    return false;
    }
    
    
    public static int valueCount(BinaryTree tree, int value) {
    	
    	if (tree != null) {
    		
    		if (value == tree.value) {
    			++count;
    		}
    		
    		valueCount(tree.left, value);
    		valueCount(tree.right, value);
    	}
    	
    	return count;
    }
	
	
    // .:: Tests if two trees are equal, same values and same arrangement ::.
    public static boolean equal(BinaryTree tree, BinaryTree tree2) {
    	
		if ((tree == null) && (tree2 == null)) {
		    return true;
		}
		
		if ((tree == null) && (tree2 != null)) {
		    return false;
		}
		
		if ((tree != null) && (tree2 == null)) {
		    return false;
		}
		
		// At this point, a and b != null, we can access their fields
		if (tree.value != tree2.value) {
		    return false;
		}
		
		return (equal(tree.left, tree2.left) && equal(tree.right, tree2.right));
    }

    
    public static boolean isTree(BinaryTree tree) {
    	
		if (tree == null) {
		    return true;
		}
		
		if ((tree.left != null) && (tree.left.value > tree.value)) {
		    return false;
		}
		
		if ((tree.right != null) && (tree.value > tree.right.value)) {
		    return false;
		}
		
		return (isTree(tree.left) && isTree(tree.right));
    }
 
    
    // ..:: Insére un élement dans l'Arbre ::.

    public void insert(int value) {
    	
		if (value == getValue()) {
		    return;  // la valeur est deja dans l'arbre
		}
		
		if (value < getValue()) {
		    if (getSousArbreGauche() != null) {
		    	getSousArbreGauche().insert(value);
		    } else {
		    	gauche = new BinaryTree(value);
		    }
		}
		
		if (value > getValue()) {
			
		    if (getSousArbreDroit() != null) {
		    	getSousArbreDroit().insert(value);
		    } else {
		    	droit = new BinaryTree(value);
		    }
		}
	}
	
	// .:: Returns how many elements there are in the tree ::.
	public static int length(BinaryTree tree) {

		if (tree != null) {
    		
			++count;
			
    		length(tree.left);
    		length(tree.right);
    	}
    	
    	return count;
	}



    // .:: Main function ::.
    public static void main(String[] arg) {
		
		// .:: Exemple d'utilisation d'un Arbre ::.

        BinaryTree b = new BinaryTree(8, new BinaryTree(45), new BinaryTree(2));
        BinaryTree c = new BinaryTree(2, new BinaryTree(800, new BinaryTree(950), null), new BinaryTree(20));       
        BinaryTree a = new BinaryTree(6, b, c);
		
		BinaryTree d = new BinaryTree(5, 
			new BinaryTree(1, 
				new BinaryTree(5), 
				new BinaryTree(10, 
					new BinaryTree(50), null)), 
			
			new BinaryTree(2, 
				null, new BinaryTree(2, 
						new BinaryTree(7), null)));

		BinaryTree e = new BinaryTree(10, d, a);

		//System.out.println(equal(a, a));

		//System.out.println(isTree(a));

		//a.insert(7);

		//System.out.println(sum(d));
		//System.out.println(max(d));
        //System.out.println(min(d));
		
		//Arbre.prefixe(d);
		//Arbre.infixe(d);
		//Arbre.postfixe(d);
		
		//System.out.println(BinaryTree.height(d));

		//System.out.println(BinaryTree.valueExists(b, 8));
		//System.out.println(BinaryTree.valueCount(a, 8));
		
		System.out.println(BinaryTree.length(d));
    }
}