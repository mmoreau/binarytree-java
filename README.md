# BinaryTree

## Script
```java
    public static void main(String[] arg) {
		
		// .:: Exemple d'utilisation d'un Arbre ::.

        BinaryTree b = new BinaryTree(8, new BinaryTree(45), new BinaryTree(2));
        BinaryTree c = new BinaryTree(2, new BinaryTree(800, new BinaryTree(950), null), new BinaryTree(20));       
        BinaryTree a = new BinaryTree(6, b, c);
		
		BinaryTree d = new BinaryTree(5, 
			new BinaryTree(1, 
				new BinaryTree(5), 
				new BinaryTree(10, 
					new BinaryTree(50), null)), 
			
			new BinaryTree(2, 
				null, new BinaryTree(2, 
						new BinaryTree(7), null)));

		BinaryTree e = new BinaryTree(10, d, a);

		//System.out.println(equal(a, a));

		//System.out.println(isTree(a));

		//a.insert(7);

		//System.out.println(sum(d));
		//System.out.println(max(d));
        //System.out.println(min(d));
		
		//Arbre.prefixe(d);
		//Arbre.infixe(d);
		//Arbre.postfixe(d);
		
		//System.out.println(BinaryTree.height(d));

		//System.out.println(BinaryTree.valueExists(b, 8));
		//System.out.println(BinaryTree.valueCount(a, 8));
		
		System.out.println(BinaryTree.length(d));
    }
```